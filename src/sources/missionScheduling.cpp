
#include "missionScheduling.h"

using namespace std;

int Submission::readParameters(const pugi::xml_node &submission)
{
    //Flush values
    id=-1;
    loop=false;
    duration=-1;
    battery=-1;
    arucoId=-1;

    //Read parameters
    string readingValue;

    //description
    pugi::xml_attribute attr;
    if (attr = submission.attribute("description")) // attribute description exists
    {
        description = attr.value();
        //std::cout << "read as string: description=" << attr.value() << std::endl;
    }

    //id
    readingValue=submission.child("config").child_value("id");
    id = atoi(readingValue.c_str());

    //loop
    readingValue=submission.child("config").child_value("loop");
    loop = atoi(readingValue.c_str());

    //duration
    readingValue=submission.child("config").child_value("time");
    istringstream convertdurationSubmision(readingValue);
    convertdurationSubmision>>duration;

    //battery
    readingValue=submission.child("config").child_value("battery");
    battery =  atoi(readingValue.c_str());

    //arucoId
    readingValue=submission.child("config").child_value("arucoId");
    arucoId =  atoi(readingValue.c_str());
    return 1;
}

int Task::readParameters(const pugi::xml_node &task)
{
    //// Flush Values ////

    //point to move
    point.clear();
    point.resize(3);
    pointLoaded.resize(3);
    for(unsigned int i=0;i<pointLoaded.size();i++)
        pointLoaded.at(i)=false;

    //speed to move
    speed.clear();
    speed.resize(3);
    speedLoaded.resize(3);
    for(unsigned int i=0;i<speedLoaded.size();i++)
        speedLoaded.at(i)=false;

    //Yaw
    yawLoaded=false;

    //Point to look
    pointToLook.clear();
    pointToLook.resize(2);
    pointToLookLoaded.resize(2);
    for(unsigned int i=0;i<pointToLookLoaded.size();i++)
        pointToLookLoaded.at(i)=false;


    //mission type
    type=missionSchedulerType::unknown;


    //// Read Common Parameters ////

    Submission::readParameters(task);

    //// Specific Parameters ////

    string readingValue;

    //command = task type
    readingValue=task.child_value("command");

    if (readingValue==CMD_TAKEOFF)
    {
        type=missionSchedulerType::takeOff;
    }
    else if(readingValue==CMD_HOVER)
    {
        type=missionSchedulerType::hover;
    }
    else if(readingValue==CMD_LAND)
    {
        type=missionSchedulerType::land;
    }
    else if(readingValue==CMD_TRAJ_MOVEMENT)
    {
        type=missionSchedulerType::trajectoryMovement;
    }
    else if(readingValue==CMD_TRAJ_MOVEMENT_OBS_AVOID)
    {
        type=missionSchedulerType::trajectoryMovement;
    }
    else if(readingValue==CMD_TRAJ_MOVEMENT_EMERGENCY)
    {
        type=missionSchedulerType::emergencyMovement;
    }
    else if(readingValue==CMD_MOVEMENT_POSITION)
    {
        type=missionSchedulerType::positionMovement;
    }
    else if(readingValue==CMD_MOVEMENT_SPEED)
    {
        type=missionSchedulerType::speedMovement;
    }
    else if(readingValue==CMD_MOVEMENT_VS_OBS_AVOID)
    {
        type=missionSchedulerType::VSMovement;
    }
    else if(readingValue==CMD_FLIP_FRONT)
    {
        type=missionSchedulerType::flipMovementFront;
    }
    else if(readingValue==CMD_FLIP_BACK)
    {
        type=missionSchedulerType::flipMovementBack;
    }
    else if(readingValue==CMD_FLIP_RIGHT)
    {
        type=missionSchedulerType::flipMovementRight;
    }
    else if(readingValue==CMD_FLIP_LEFT)
    {
        type=missionSchedulerType::flipMovementLeft;
    }
    else if(readingValue==CMD_SLEEP)
    {
        type=missionSchedulerType::sleep;
    }
    else
    {
        type=missionSchedulerType::unknown;
    }

    string speechValue;
    speechValue = task.child_value("speech");
    speechSent = speechValue;


    //Modules Needed

    pugi::xml_node moduleValue;
    modules.clear();

    for (moduleValue = task.child("modules").first_child();moduleValue ; moduleValue = moduleValue.next_sibling()){
        modules.push_back(moduleValue.child_value());

    }

    //Mission Point
    //x
    readingValue=task.child("point").child_value("x");
    if(readingValue!="")
    {
        pointLoaded.at(0)=true;
        istringstream convertxtask(readingValue);
        convertxtask>>point.at(0);
    }

    //y
    readingValue=task.child("point").child_value("y");
    if(readingValue!="")
    {
        pointLoaded.at(1)=true;
        istringstream convertytask(readingValue);
        convertytask>>point.at(1);
    }

    //z
    readingValue=task.child("point").child_value("z");
    if(readingValue!="")
    {
        pointLoaded.at(2)=true;
        istringstream convertztask(readingValue);
        convertztask>>point.at(2);
    }

    //Mission Speed
    //dx
    readingValue=task.child("speed").child_value("x");
    if(readingValue!="")
    {
        speedLoaded.at(0)=true;
        istringstream convertxtask(readingValue);
        convertxtask>>speed.at(0);
    }

    //dy
    readingValue=task.child("speed").child_value("y");
    if(readingValue!="")
    {
        speedLoaded.at(1)=true;
        istringstream convertytask(readingValue);
        convertytask>>speed.at(1);
    }

    //dz
    readingValue=task.child("speed").child_value("z");
    if(readingValue!="")
    {
        speedLoaded.at(2)=true;
        istringstream convertztask(readingValue);
        convertztask>>speed.at(2);
    }


    //yaw
    readingValue=task.child_value("yaw");
    if(readingValue!="")
    {
        yawLoaded=true;
        istringstream convertyawSubmision(readingValue);
        convertyawSubmision>>yaw;
        yaw*=(M_PI/180.0);
    }


    //point to look
    //x
    readingValue=task.child("pointToLook").child_value("x");
    if(readingValue!="")
    {
        pointToLookLoaded.at(0)=true;
        istringstream convertxtask(readingValue);
        convertxtask>>pointToLook.at(0);
    }

    //y
    readingValue=task.child("pointToLook").child_value("y");
    if(readingValue!="")
    {
        pointToLookLoaded.at(1)=true;
        istringstream convertytask(readingValue);
        convertytask>>pointToLook.at(1);
    }

    //if both loaded, we discard yaw loaded!!
    if(pointToLookLoaded.at(0) && pointToLookLoaded.at(1) && yawLoaded)
        yawLoaded=false;


    return 1;
}


RobotMissionPlanner::RobotMissionPlanner()
{
    init();
    return;
}

RobotMissionPlanner::~RobotMissionPlanner()
{
    clear();
    return;
}

int RobotMissionPlanner::init()
{
    return 1;
}

int RobotMissionPlanner::clear()
{
    return 1;
}




int RobotMissionPlanner::setMission(std::string missionConfigFile)
{
    //xml parser

    std::ifstream nameFile(missionConfigFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);
    if(!result)
        return 0;

    //cout<<" .Opened Schedule"<<missionConfigFile<<endl;

    //read general parameters of the mission
    pugi::xml_node config=doc.child("mission").child("config");
    std::string readingValue;

    defaultPrecission.resize(3);


    //x
    readingValue=config.child("precission").child_value("x");
    if(readingValue!="")
    {
        istringstream convertxprecission(readingValue);
        convertxprecission>>defaultPrecission.at(0);
    }

    //y
    readingValue=config.child("precission").child_value("y");
    if(readingValue!="")
    {
        istringstream convertyprecission(readingValue);
        convertyprecission>>defaultPrecission.at(1);
    }

    //z
    readingValue=config.child("precission").child_value("z");
    if(readingValue!="")
    {
        istringstream convertzprecission(readingValue);
        convertzprecission>>defaultPrecission.at(2);
    }

    //yaw
    readingValue=config.child("precission").child_value("yaw");
    if(readingValue!="")
    {
        istringstream convertzprecission(readingValue);
        convertzprecission>>yawPrecission;
    }
    else
        yawPrecission=10.0;


    yawPrecission*=M_PI/180.0;

    if(!generateMissionTree())
        return 0;

    //end
    return 1;
}


int RobotMissionPlanner::generateMissionTree()
{
    //clear
    submissionTree.clear();
    taskTree.clear();
    submissionTreeDescription.clear();
    taskTreeDescription.clear();


    //aux vars
    std::vector<int> tasksInSubmission;
    std::vector<std::string> tasksDescriptionsInSubmission;
    int readId;
    int aruco;
    string readingValue;
    string description;
    string arucoValue;


    //generate submission tree
    cout << "\033[1;31mMission Schedule Tree Generated\033[0m" << endl;

    pugi::xml_node submission;
    for(submission = doc.child("mission").child("submission"); submission; submission = submission.next_sibling("submission"))
    {
        //read submission id
        readingValue=submission.child("config").child_value("id");
        istringstream convertidSubmision(readingValue);
        convertidSubmision>>readId;

        pugi::xml_attribute attr;
        if (attr = submission.attribute("description")) // attribute description exists
        {
            description = attr.value();
            //std::cout << "read as string: description=" << attr.value() << std::endl;
        }

        arucoValue = submission.child("config").child_value("arucoId");
        //if (arucoValue.size() != 0)
        //{
            aruco = atoi(arucoValue.c_str());

            arucoIdVector.push_back(aruco);
        //}

        //add description of the submission to the submission tree
        submissionTreeDescription.push_back(description);



        if(std::find(submissionTree.begin(), submissionTree.end(), readId) != submissionTree.end())
        {
            cout << "\033[1;31m/////////////SUBMISSION ID REPEATED/////////////\033[0m" << endl;
        }


        //add id of the submission to the submission tree
        submissionTree.push_back(readId);

    }
    //sort
    std::sort(submissionTree.begin(),submissionTree.end());


    //generate task tree
    for(unsigned int i=0;i<submissionTree.size();i++)
    {
        //cout << "tree description: " << submissionTreeDescription.at(i) << endl;

        cout << "\033[1;34m....Submission: \033[0m" << "\033[1;32m" << submissionTreeDescription.at(i) << "\033[0m"<< endl;

        //search the submission using id
        bool flagSubmissionFound=false;
        for(submission = doc.child("mission").child("submission"); submission; submission = submission.next_sibling("submission"))
        {
            //read submission id
            int readId;
            string readingValue=submission.child("config").child_value("id");
            istringstream convertidSubmision(readingValue);
            convertidSubmision>>readId;
            if(readId==submissionTree.at(i))
            {
                flagSubmissionFound=true;
                //cout<<"  read id="<<readId<<endl;
                break;
            }
        }

        if(flagSubmissionFound)
        {
            //read tasks
            pugi::xml_node task;
            tasksInSubmission.clear();
            tasksDescriptionsInSubmission.clear();

            for(task = submission.child("task"); task; task = task.next_sibling("task"))
            {
                //read task id
                readingValue=task.child("config").child_value("id");
                istringstream convertidTask(readingValue);
                convertidTask>>readId;

                pugi::xml_attribute attr;
                if (attr = task.attribute("description")) // attribute description exists
                {
                    description = attr.value();
                    //std::cout << "read as string: description=" << attr.value() << std::endl;
                }

                if(std::find(tasksInSubmission.begin(), tasksInSubmission.end(), readId) != tasksInSubmission.end())
                {
                    cout << "\033[1;31m/////////////TASK ID REPEATED/////////////\033[0m" << endl;
                }

                tasksDescriptionsInSubmission.push_back(description);
                tasksInSubmission.push_back(readId);

            }

            //sort
            std::sort(tasksInSubmission.begin(),tasksInSubmission.end());

            for(unsigned int j=0;j<tasksDescriptionsInSubmission.size();j++)
            {
                cout << "\033[1;35m........Task: \033[0m" << "\033[1;37m" << tasksDescriptionsInSubmission.at(j) << "\033[0m"<< endl;
                //cout << "\033[1;35m..........ID: \033[0m" << "\033[1;37m" << tasksInSubmission.at(j) << "\033[0m"<< endl;

            }

            //push back

            taskTreeDescription.push_back(tasksDescriptionsInSubmission);
            taskTree.push_back(tasksInSubmission);
        }

    }


    return 1;
}


int RobotMissionPlanner::readTask(int submissionIdIn, int taskIdIn)
{

    //search submission
    pugi::xml_node submission;
    bool flagSubmissionFound=false;
    for(submission = doc.child("mission").child("submission"); submission; submission = submission.next_sibling("submission"))
    {
        //read submission id
        int readId;
        string readingValue=submission.child("config").child_value("id");
        istringstream convertidSubmision(readingValue);
        convertidSubmision>>readId;
        if(readId==submissionIdIn)
        {
            flagSubmissionFound=true;
            break;
        }
    }

    //search task
    pugi::xml_node task;
    bool flagTaskFound=false;
    for(task = submission.child("task"); task; task = task.next_sibling("task"))
    {
        //read task id
        int readId;
        string readingValue=task.child("config").child_value("id");
        istringstream convertidTask(readingValue);
        convertidTask>>readId;
        if(readId==taskIdIn)
        {
            flagTaskFound=true;
            break;
        }
    }


    if(!flagSubmissionFound || !flagTaskFound)
    {
        return 0;
    }

    //read parameters of the task

    TheSubmission.readParameters(submission);
    TheTask.readParameters(task);

    return 1;
}




