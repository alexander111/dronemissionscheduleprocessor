/*
 * 
 *
 *  Created on: 
 *      Author: Ramon Suarez Fernandez
 */

#ifndef MISSION_SCHEDULING_H
#define MISSION_SCHEDULING_H




//Math
#include <math.h>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <algorithm>

//ifstream
#include <fstream>

//XML parser
#include "pugixml.hpp"

//Old Commands
#define CMD_TAKEOFF                     "takeOff"
#define CMD_HOVER                       "hover"
#define CMD_LAND                        "land"
#define CMD_TRAJ_MOVEMENT               "moveTraj"
#define CMD_SLEEP                       "sleep"

//New Commands
#define CMD_FLIP_RIGHT                  "moveFlipRight"
#define CMD_FLIP_LEFT                   "moveFlipLeft"
#define CMD_FLIP_FRONT                  "moveFlipFront"
#define CMD_FLIP_BACK                   "moveFlipBack"
#define CMD_EMERGENCY                   "emergency"
#define CMD_SEARCH                      "search"

#define CMD_TRAJ_MOVEMENT_OBS_AVOID     "moveTrajObsAvoid"  //Assuming Aruco
#define CMD_TRAJ_MOVEMENT_EMERGENCY     "moveTrajObsAvoidEmergency" //move in emergency mode
#define CMD_MOVEMENT_VS                 "moveVS"
#define CMD_MOVEMENT_VS_OBS_AVOID       "moveVSObsAvoid"
#define CMD_MOVEMENT_MAN_THRUST         "moveManThrust"
#define CMD_MOVEMENT_MAN_ALT            "moveManAlt"
#define CMD_MOVEMENT_SPEED              "moveSpeed"
#define CMD_MOVEMENT_POSITION           "movePosition"






namespace missionSchedulerType
{
    //tasks types
    enum taskTypes
    {
        takeOff,                    //take off
        hover,                      //hover
        land,                       //land
        sleep,                      //wait there
        trajectoryMovement,         //move in trajectory mode
        emergencyMovement,          //move in emergency mode
        positionMovement,           //move in position mode
        unknown,
        speedMovement,              //move in speed mode
        VSMovement,                 // move in VS
        flipMovementRight,          // move in flip mode right
        flipMovementLeft,          // move in flip mode left
        flipMovementFront,          // move in flip mode front
        flipMovementBack,          // move in flip mode back
        emergency                   //emergency mode

    };
}



class Submission
{
public:

    int id;
    int arucoId;
    bool loop;
    double duration;
    double battery;

    std::string description;

public:
    int readParameters(const pugi::xml_node &submission);

};


class Task : public Submission
{
public:
    //Task Type
    missionSchedulerType::taskTypes type;

    //new 30/07/2015
    int id_movement; //To know if task is trajectory, VS, Speed, Position

    //Yaw angle
    double yaw;
    bool yawLoaded;

    std::string speechSent;
    //Point to go
    std::vector<double> point;
    std::vector<bool> pointLoaded;

    //point to look
    std::vector<double> pointToLook;
    std::vector<bool> pointToLookLoaded;

    //Speed to go
    std::vector<double> speed;
    std::vector<bool> speedLoaded;

    //Modules Needed
    std::vector<std::string> modules;



public:
    int readParameters(const pugi::xml_node &task);

};


class RobotMissionPlanner
{

private:
    pugi::xml_document doc;

public:
    RobotMissionPlanner();
    ~RobotMissionPlanner();

public:
    int init();
    int clear();
    int generateMissionTree();
    int setMission(std::string missionConfigFile);

public:
    std::vector<int> submissionTree;            //tree with the id of the submissions
    std::vector< std::vector<int> > taskTree;   //tree with the id of the tasks of each submission
    std::vector<std::string> submissionTreeDescription;
    std::vector< std::vector<std::string> > taskTreeDescription;
    std::vector<int> arucoIdVector;
    std::vector<double> precission;

    //general
public:
    std::vector<double> defaultPrecission;
    double yawPrecission;

    //Submission
public:
    Submission TheSubmission;

    //task
public:
    Task TheTask;

public:
    int readTask(int submissionIdIn, int taskIdIn);

};

#endif
